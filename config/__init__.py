# Define any custom Jinja2 functions here
from flask import url_for
from .config import URL_PREFIX

# Get around the routing prefix issue inside the templates
def url_for2(endpoint: str, **kwargs):
    return URL_PREFIX + str(url_for(endpoint, **kwargs))


CUSTOM_TEMPLATE_FUNCTIONS = {
    "url": url_for2,
}
