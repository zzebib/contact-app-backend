import os

# This should match exactly the name of the app you specified
APP_NAME = "contact-db"

ENV = os.environ.get('ENV', 'prod').lower()
URL_PREFIX = f"/{APP_NAME}" if ENV == 'prod' else ""


# Get the app configuration based on the ENV environment variable (default is prod)
def get_app_config():
    if ENV == 'prod':
        return ProductionConfig()
    else:
        return DevelopmentConfig()


# If you have created a database for this app, the connection string will be automatically
# accessible through the DATABASE_URL environment variable.
def get_db_url(testing=False):

    url = os.environ.get('DATABASE_URL')
    if url is None:
        print("WARNING: Could not connect to the given database URL!")

    # For PostgreSQL databases, the conn string needs to start with "postgresql"
    if url and url.startswith("postgres://"):
        url = url.replace("postgres://", "postgresql://", 1)

    return url


class CommonConfig:
    APP_NAME = APP_NAME
    URL_PREFIX = URL_PREFIX
    
    SECRET_KEY = "my-secrefasdfasdfasdfasdt-key"
    SESSION_COOKIE_SECURE = True
    SQLALCHEMY_DATABASE_URI = get_db_url()
    SQLALCHEMY_TRACK_MODIFICATIONS = False


# Flask App settings for production environment
class ProductionConfig(CommonConfig):
    DEBUG = False
    APPLICATION_ROOT = f"/{APP_NAME}"


# Flask App settings for local development enviroment
class DevelopmentConfig(CommonConfig):
    DEBUG = True

# Settings for running tests
class TestConfig(CommonConfig):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = get_db_url(testing=True)