from database.db import db , ma



class Entity(db.Model):
    def __init__(self, name, age):
        super(Entity, self).__init__(name = name)
        super(Entity, self).__init__(age = age)


    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=False, nullable=False)
    age = db.Column(db.Integer, unique=False, nullable=False)

    def __repr__(self):
        return f'Entity({self.id}, {self.name}, {self.age})'

class EntitySchema(ma.Schema):
    class Meta:
        fields = ("id", "name", "age")
        model = Entity


entity_schema = EntitySchema()
