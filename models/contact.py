from database.db import db , ma



class Contact(db.Model):

    def __init__(self, name, organization , number, country, email, jobTitle):
        super(Contact, self).__init__(name = name)
        super(Contact, self).__init__(organization = organization)
        super(Contact, self).__init__(number = number)
        super(Contact, self).__init__(country = country)
        super(Contact, self).__init__(email = email)
        super(Contact, self).__init__(jobTitle = jobTitle)

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    organization = db.Column(db.String(80), nullable=False)
    number = db.Column(db.String(80), nullable=False)
    country = db.Column(db.String(80),  nullable=False)
    email = db.Column(db.String(80),  nullable=False)
    jobTitle = db.Column(db.String(80), nullable=False)



class ContactSchema(ma.Schema):
    class Meta:
        fields = ("id", "name", "organization" , "number" , "country", "email", "jobTitle")
        model = Contact

contact_schema = ContactSchema()
contacts_schema = ContactSchema(many=True)


