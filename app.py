import email
from itertools import count
from unicodedata import name
from urllib import request
from flask import Flask , request ,jsonify

from database.db import db , ma
from config.config import ENV, get_app_config

# Create and configure our Flask app
app = Flask(__name__)
app.config.from_object(get_app_config())
db.init_app(app)
ma.init_app(app)



# Import all your database models just like below:
with app.app_context():
    from models.entity import Entity , entity_schema
    from models.contact import Contact , contact_schema , contacts_schema
    if app.config["SQLALCHEMY_DATABASE_URI"]:
        db.create_all()



@app.route('/hello', methods=['GET'])
def hello_world():
    return "Hello World!"


@app.route('/addContact', methods=['POST'])
def add_contact():
    name = request.json["name"]
    organization = request.json["organization"]
    number = request.json["number"]
    email = request.json["email"]
    jobTitle = request.json["jobTitle"]
    country = request.json["country"]
    

    new_contact = Contact(name=name, number=number, organization=organization, email= email,jobTitle=jobTitle, country= country )
    db.session.add(new_contact)
    db.session.commit()
    return jsonify(contact_schema.dump(new_contact))



@app.route('/getContact', methods=['GET'])
def get_contact():
    contacts = Contact.query.all()
    return jsonify(contacts_schema.dump(contacts))











if __name__ == '__main__':
    app.run()
